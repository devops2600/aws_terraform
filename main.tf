

provider "aws" {
  region = "ap-south-1"
}
resource "aws_instance" "AWSEC2Instance" {
  ami             = "ami-0d2986f2e8c0f7d01"
  instance_type   = "t2.micro"
  tags = {
    Name = "EC2-Instance by terraform"
  }
}
